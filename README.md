# VOEB Topic modelling (LV Th. Wallnig, WS 2021)

## Preconditions 

Python packages:

* pandas
* mathplotlib


* spacy 2.7 (current 3.1 does not work yet) (https://spacy.io)
* gensim < 4.0 (https://radimrehurek.com/gensim)

For spacy you have to do `python -m spacy download de_core_news_sm` to retrieve
the trained data.



## Links

If you want to know a bit more, have a look at these pages:

* https://www.machinelearningplus.com/nlp/topic-modeling-gensim-python/

* https://www.machinelearningplus.com/nlp/topic-modeling-visualization-how-to-present-results-lda-models


and directly from the horses' mouth:

* https://www.youtube.com/watch?v=FkckgwMHP2s


and if you want to play a little, try the fake news dataset from Kaggle (5 years old):

* https://www.kaggle.com/mrisdal/fake-news
