#
# A module to bundle recurring functions for topic
# modelling
#
# working copy in jbekesi
# copy for all in /scratch, later on as package/wheel/lib


import datetime
import json
from pathlib import Path
from pprint import pprint
from operator import itemgetter
import subprocess
import shlex

import gensim
import pandas as pd
import spacy

import matplotlib.pyplot as plt

# import nltk #  do that later
from gensim import corpora
from gensim.utils import simple_preprocess
from gensim.models.coherencemodel import CoherenceModel

# Initialize spacy 'de' model, keeping only tagger component (for efficiency)
# python3 -m spacy download en
# instead of this:
# nlp = spacy.load('de', disable=['parser', 'ner'])
# we use this:
try:
    import de_core_news_sm
    nlp = de_core_news_sm.load(disable=['parser', 'ner'])
except:
    print("please load de_core_news_sm for lemmatization")
    nlp = None

NOW = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    
# search for stopwordfile
def get_stopwords():
    curpath = Path.cwd()
    swf = "stopwords_de.txt"
    for search in (".", ".."):
        if curpath.joinpath(search, swf).is_file():
            return curpath.joinpath(search, swf).read_text().split("\n")
    for search in curpath.iterdir():
        if search.is_dir() and search.joinpath(swf).is_file():
            return search.joinpath(swf).read_text().split("\n")
    print("no stopword file {} found".format(swf))
    return []

# alternate stopword access
# nltk.download('stopwords')
# from nltk.corpus import stopwords
# stop_words = stopwords.words('german')

# Build the bigram and trigram models

#bigram = gensim.models.Phrases(data_words, min_count=5, threshold=100) # higher threshold fewer phrases.
#trigram = gensim.models.Phrases(bigram[data_words], threshold=100)  

# Faster way to get a sentence clubbed as a trigram/bigram
#bigram_mod = gensim.models.phrases.Phraser(bigram)
#trigram_mod = gensim.models.phrases.Phraser(trigram)

# Define functions for stopwords, bigrams, trigrams and lemmatization

    
def remove_stopwords(texts, include=[]):
    stop_words = []
    stop_words = get_stopwords()
    if include:
        if type(include) == type([]):
            stop_words += include
        else:
            print("include has to be type of 'list', skipping")
    return [[word for word in simple_preprocess(str(doc)) if word not in stop_words] for doc in texts]

def make_bi_trigrams(data_words):
    bigram = gensim.models.Phrases(data_words, min_count=5, threshold=100) # higher threshold fewer phrases.
    trigram = gensim.models.Phrases(bigram[data_words], threshold=100)
    # Faster way to get a sentence clubbed as a trigram/bigram
    bigram_mod = gensim.models.phrases.Phraser(bigram)
    trigram_mod = gensim.models.phrases.Phraser(trigram)
    bigrams_out = [bigram_mod[doc] for doc in data_words]
    trigrams_out = [trigram_mod[bigram_mod[doc]] for doc in data_words]
    return bigrams_out, trigrams_out 


def lemmatization(texts, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
    """https://spacy.io/api/annotation"""
    texts_out = []
    for sent in texts:
        doc = nlp(" ".join(sent)) 
        texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
    return texts_out


def save_lemmatized(data_lemmatized, corpusname="", datadir=None):
    "if no corpusname or datadir save here for later use"
    if not datadir:
        datadir = Path.cwd()
    if corpusname:
        corpusname = "{}_".format(corpusname)
    else:
        print("no corpusname in saving lemmas, hope that's ok.")
    filename = datadir.joinpath("lemmatized_{}{}.json".format(corpusname, NOW))
    with open(filename, 'w') as dlj:
        json.dump(data_lemmatized, dlj)

        
def get_lemmatized(corpusname="", datadir=None):
    "if no corpusname or datadir save here for later use"
    if not datadir:
        datadir = Path.cwd()
    if corpusname:
        corpusname = "{}_".format(corpusname)
    else:
        print("no corpusname in retrieving lemmas, hope that's ok.")
    try:
        print("corpusname:", corpusname)
        filename = list(sorted(datadir.glob("lemmatized_{}*.json".format(corpusname))))[-1]
    except Exception as err:
        print("ERROR", err, datadir, corpusname)
        return []
    with open(filename) as dlj:
        return json.load(dlj)   
        
        
def get_corpus_dictionary(data_lemmatized, corpusname="", save=True, 
                          datadir=None, from_file=False):
    "saving corpus for later use, returns dictionary and corpus"
    if not datadir:
        datadir = Path.cwd()
    if corpusname:
        orgcorpus = corpusname
        corpusname = "{}_{}".format(corpusname, NOW)
    else:
        print("no corpusname in saving corpus/dictionary, hope that's ok.")
        corpusname = NOW

    if from_file:
        print(corpusname, orgcorpus)
        # retrieve corpus from file, use last one
        dictfile = str(sorted(datadir.glob("dict_{}*.dict".format(orgcorpus)))[-1])
        corpfile = str(sorted(datadir.glob("corpus_{}*.mm".format(orgcorpus)))[-1])
        id2word = corpora.Dictionary().load(dictfile)
        corpus = corpora.MmCorpus(corpfile)
        print("loading dict and corpus from {}, {}".format(dictfile, corpfile))
        return id2word, corpus
    # OR:
    # Create Dictionary
    id2word = corpora.Dictionary(data_lemmatized)
    # Create Corpus
    # Term Document Frequency
    corpus = [id2word.doc2bow(text) for text in data_lemmatized]
    if save:
        filename_dict = str(datadir.joinpath('dict_{}.dict'.format(corpusname)))
        print(filename_dict)
        id2word.save(filename_dict) # store the dictionary, for future reference
        filename_corp = str(datadir.joinpath('corpus_{}.mm'.format(corpusname)))
        corpora.MmCorpus.serialize(filename_corp, corpus) # save corpus in MarketMatrix format   
        print("saved dictionary and corpus in MarketMatrix format")
    return id2word, corpus
    
def csv_to_datasette(tablename="newTable", csv="", db=None):
    # importing/overwriting csv data into/in datasette db
    # csv has to be full path, e.g. str(Path.cwd().joinpath("foo"))
    # trying to insert csv dominant_topic into datasette-db
    # default for db name is login name of user
    output_table = tablename
    if not output_table:
        print("no table name, skipping")
        return
    csvf = csv
    if not db:
        # try to use loginname as base for db
        db = Path.home().stem.split("-")[-1]
    cmd = 'csvs-to-sqlite -s ";" --replace-tables -t {} {} /opt/tljh/datasette/db/{}.db'
    cmd = cmd.format(output_table, str(csvf), db)
    args = shlex.split(cmd)
    out = subprocess.run(args, stdout=subprocess.PIPE)
    if out.returncode == 0:
        print("Table {} in {}.db updated/created".format(output_table, db))
    else:
        print("an error occurred, please revise!")
        print(out)    
    
    
def compute_coherence(corpus, id2word, texts, corpusname="noCorpusname", start=8, stop=33, step=4,
                     datadir=None, to_db=False):
    # find optimal number of topics by computing coherence values
    #  %matplotlib inline
    if not datadir:
        print("no datadir given, stopping")
        return
    models = []
    print("computing coherence for the model starting with {}, "
                  "interval {} and stopping at {}".format(start, step, stop))
    for num_topics in range(start, stop, step):
        print("computing lda num topics of", num_topics)
        lda_model = gensim.models.LdaModel(corpus=corpus,
                                           id2word=id2word,
                                           num_topics=num_topics,
                                           random_state=35009)
        cm = CoherenceModel(model=lda_model, corpus=corpus, texts=texts, coherence="c_v")
        models.append((num_topics, cm.get_coherence(), lda_model.show_topics(), cm, lda_model))    

    # Show graph
    x = range(start, stop, step)
    plt.plot(x, [m[1] for m in models])
    plt.xlabel("Num Topics")
    plt.ylabel("LDA coherence score")
    plt.legend(("coherence_values"), loc='best')
    plt.show()
    for m in models:
        print("topics:", m[0], "coherence: ", m[1])

    # comparison of topics:
    # store it  in a csv file for later perusal

    df = pd.DataFrame()
    for m in models:
        print("===", m[0])
        # print(m[2]) # what we did store earlier
        for topic_num in range(m[0]):
            wp = m[4].show_topic(topic_num)
            topic_keywords = [word for word, prop in wp]
            # print("   ", ", ".join(topic_keywords))
            df = df.append(pd.Series([int(m[0]), "t.{}".format(topic_num + 1)] + topic_keywords), ignore_index=True)
    if not to_db:
        return
    df.columns = ['Number_topics', 'Topic_number']  + ["word_{}".format(x) for x in range(1,11)]
    tablename = "lda_{}_coherence_topics_{}-{}.csv".format(corpusname,start,stop)
    filename = str(datadir.joinpath("{}.csv".format(tablename)))
    df.to_csv(filename, sep=";", index=False)
    csv_to_datasette(tablename, filename)
    print("Choosing a topic number for a model that marks the end of a rapid growth "
          "of topic coherence usually offers meaningful and interpretable topics. "
          "Picking an even higher value can sometimes provide more granular sub-topics.")
    

def format_topics_sentences(ldamodel=None, corpus=None, texts=None):
    # cf. `testing_103.ipynb`
    sent_topics_df = pd.DataFrame()
    topics = {}
    # Get main topic in each document
    for i, row in enumerate(ldamodel[corpus]):
        row_item = row[0]
        print(row_item)
        row_item = sorted(row_item, key=itemgetter(1), reverse=True)[0]
        topic_num, prop_topic = row_item
        if not topic_num in topics:
            wp = ldamodel.show_topic(topic_num)
            topic_keywords = ", ".join([word for word, prop in wp])
            topics[topic_num] = topic_keywords
        else:
            topic_keywords = topics[topic_num]
        sent_topics_df = sent_topics_df.append(pd.Series([int(topic_num), round(prop_topic,4), 
                                    str(topic_num)+": " + topic_keywords]), ignore_index=True)
 
    sent_topics_df.columns = ['Dominant_Topic', 'Perc_Contribution', 'Topic_Keywords']
    contents = pd.Series(texts)
    sent_topics_df = pd.concat([sent_topics_df, contents], axis=1)
    sent_topics_df = sent_topics_df['Dominant_Topic'].fillna(0).apply(lambda x: str(int(x)))
    return(sent_topics_df)


def convertldaGenToldaMallet(mallet_model):
    """converts mallet model to gensim (for use in pyLDAvis)
    (source: https://neptune.ai/blog/pyldavis-topic-modelling-exploration-tool-that-every-nlp-data-scientist-should-know)
    """
    model_gensim = gensim.models.LdaModel(
        id2word=mallet_model.id2word, num_topics=mallet_model.num_topics,
        alpha=mallet_model.alpha, eta=0,
    )
    model_gensim.state.sstats[...] = mallet_model.wordtopics
    model_gensim.sync_state()
    return model_gensim